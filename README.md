# SpringBootSecurityDemo

#### 介绍
spring security 公钥验证测试

由认证服务器jwk公钥解析验证token
 * 流程：
 * 1、通过header获取token（jwt格式的）
 * 2、去服务器提取公钥
 * 3、缓存到本地
 * 4、解析token

- jwt官网：token 网页验证地址：https://jwt.io/#decoded-jwt
