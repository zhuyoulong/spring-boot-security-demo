//package com.iv.demo.controller;
//
//import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
///**
// * <p>Description:  </p>
// *
// * @author zhuyoulong
// * @date 2020-12-30
// */
//
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
////    @Value(value = "${auth0.apiAudience}")
////    private String apiAudience;
////    @Value(value = "${auth0.issuer}")
////    private String issuer;
//
////    @Autowired
////    private UserDetailsService userDetailsService;
////
////    @Override
////    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////        auth.userDetailsService(userDetailsService);
////    }
//
////    @Bean
////    public JwtAccessTokenConverter accessTokenConverter() {
////        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
////        converter.setSigningKey("123");
////        return converter;
////    }
////    @Override
////    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
////        endpoints.tokenStore(tokenStore())
////                .accessTokenConverter(accessTokenConverter())
////                .authenticationManager(authenticationManager);
////    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
////        JwtWebSecurityConfigurer
////                .forRS256(apiAudience, issuer)
////                .configure(http)
//////                .cors().and().csrf().disable().authorizeRequests()
////                .cors().disable().authorizeRequests()
////                .anyRequest().permitAll();
//
//String issuer = "https://test.auth.fjfuyu.net/.well-known/openid-configuration/jwks";
//String apiAudience = "edu-api";
//        JwtWebSecurityConfigurer  //Auth0 provided class performs per-authentication using JWT token
//                .forRS256(apiAudience, issuer)
//                .configure(http)
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/hello").permitAll()
//                .antMatchers(HttpMethod.GET, "/Test/authenticated").authenticated();
//    }
//}
